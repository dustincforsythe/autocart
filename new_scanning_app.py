import threading
import time
import sys
from tkinter import *

shoppingListItems = []
shoppingListLength = 0
shoppingListPrices = []
totalPrice = 0.0

storeCatalog = {"640522710850": ["Raspberry Pi 3: Model B", "34.99"],
                "022000020734": ["Extra Chewy Mints: Polar Ice", "2.41"]
                }

def callback():
    try:
        selected = groceryListNames.curselection()
        groceryListNames.delete(selected)
        groceryListPrice.delete(selected)
        groceryListTotal.delete(0,END)
        allPrices = sum(map(float,list(groceryListPrice.get(0,END))))

        groceryListTotal.delete(0,END)
        groceryListTotal.insert(END,"{0:.2f}".format(allPrices))
    
    except:
        pass

def yview(*args):
    groceryListNames.yview(*args)
    groceryListPrice.yview(*args)

class barcode_scanning_app(threading.Thread):

    def __init__(self, name):
        threading.Thread.__init__(self)
        self.name = name
            
    def run(self):
        
        if(self.name == "scanner"):

            def barcode_reader():
                code = input("Scan a barcode: ")
                code = str(code)
                return code

            def UPC_lookup(upc):
                if upc in storeCatalog:
                    return storeCatalog.get(upc)

            try:
                while True:

                    currentScan = UPC_lookup(barcode_reader())

                    shoppingListItems.append(currentScan[0])
                    shoppingListPrices.append(float(currentScan[1]))
                    totalPrice = sum(shoppingListPrices)
                    shoppingListLength +=1
                    print(shoppingListItems)
                    print(shoppingListLength)
                    #print(groceryListNames.size())
            
            except KeyboardInterrupt:
                print("ERROR")
                pass

        if(self.name == "GUI"):
            # Sets up the main widget
            root = Tk()

                
            #root.attributes("-fullscreen",True)
            #root.geometry("480x320")
            root.title("Semi-Autonomous Shopping Cart")

            # Sets up section titles
            titlescanned = Label(root,text = "Scanned Products:")
            titleprice = Label(root,text = "Price")

            # Sets up scrollbar
            scrollbar = Scrollbar(root, orient=VERTICAL, command=yview)

            # Sets up grocery names and prices listboxes
            groceryListNames = Listbox(root, width = 50, height = 15,yscrollcommand=scrollbar.set)
            groceryListPrice = Listbox(root, width = 7, height = 15,yscrollcommand=scrollbar.set, justify=RIGHT)

            # Sets up remove button
            removeButton = Button(root, text= "Remove Selected", height=4, width = 25, command=callback)

            # Sets up the price output as an empty button
            groceryListTotal = Listbox(root, width = 7, height = 1, justify=RIGHT)

            #allPrices = sum(map(float,list(groceryListPrice.get(0,END))))

            groceryListTotal.delete(0,END)
            groceryListTotal.insert(END,"{0:.2f}".format(totalPrice))
            
            if(shoppingListLength != groceryListNames.size()):
                groceryListNames.delete(0,END)
                groceryListNames.delete(0,END)
                for i in range(0,len(shoppingListItems)):
                    groceryListNames.insert(END,shoppingListItems[i])
                    groceryListPrice.insert(END,shoppingListPrices[i])

                groceryListTotal.delete(0,END)
                groceryListTotal.insert(END,"{0:.2f}".format(totalPrice))
            
            titlescanned.grid(row=0,column=0)
            titleprice.grid(row=0,column=1)
            scrollbar.grid(row=1,column=2,sticky = "ns")
            groceryListNames.grid(row=1,column=0)
            groceryListPrice.grid(row=1,column=1)
            groceryListTotal.grid(row=2,column=1,sticky = "n")
            removeButton.grid(row=2,column=0,rowspan=2)

            root.update()
            






# create new threads
thread1 = barcode_scanning_app("scanner")
thread2 = barcode_scanning_app("GUI")

thread1.start()
thread2.start