int leftMotorPWM = 10;
int leftMotorDir = 12;

int rightMotorPWM = 11;
int rightMotorDir = 13;

int fast = 255;
int slow = 255;
int med = 120;

void setup(){
  Serial.begin(9600);
  pinMode(leftMotorPWM, OUTPUT);
  pinMode(rightMotorPWM, OUTPUT);
  pinMode(leftMotorDir, OUTPUT);
  pinMode(rightMotorDir, OUTPUT);
}

void loop(){
  String value = Serial.readString();
    
    if (value.equals("left")){
      digitalWrite(rightMotorDir, HIGH);
      analogWrite(rightMotorPWM, slow);
      
      digitalWrite(leftMotorDir, LOW);
      analogWrite(leftMotorPWM, slow);
      
    }
    if (value.equals("right")){
      digitalWrite(rightMotorDir, LOW);
      analogWrite(rightMotorPWM, slow);
      
      digitalWrite(leftMotorDir, HIGH);
      analogWrite(leftMotorPWM, slow);
      
    }
    if (value.equals("forward")){
      digitalWrite(rightMotorDir, LOW);
      analogWrite(rightMotorPWM, slow);
      
      digitalWrite(leftMotorDir, LOW);
      analogWrite(leftMotorPWM, slow);
      
    }
    if (value.equals("backward")){
      digitalWrite(rightMotorDir, HIGH);
      analogWrite(rightMotorPWM, slow);
      
      digitalWrite(leftMotorDir, HIGH);
      analogWrite(leftMotorPWM, slow);
    }
    if (value.equals("stop")){
      digitalWrite(rightMotorDir, LOW);
      analogWrite(rightMotorPWM, 0);
      
      digitalWrite(leftMotorDir, HIGH);
      analogWrite(leftMotorPWM, 0);
      
    }

  delay(500);
  
}

