import BarcodeScanner
import _thread


def main():
    scanner = BarcodeScanner.BarcodeScanner()
    _thread.start_new_thread(scanner.draw())
    _thread.start_new_thread(scanner.readBarcode())
    


main()
    
