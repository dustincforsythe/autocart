class Customer():

    def __init__(self):

        shoppingListItems = []
        shoppingListPrices = []
        totalPrice = 0.0
        barcodeScanner = Scanner()
        scannedLength = 0

    def addItem(self):

        shoppingListItems.append(barcodeScanner.getName())
        shoppingListPrices.append(barcodeScanner.getPrice())
        totalPrice = sum(shoppingListPrices)
        scannedLength += 1

    def removeItem(self,location):

        shoppingListItems.pop(location)
        shoppingListPrices.pop(location)
        totalPrice = sum(shoppingListPrices)
        scannedLength -= 1

    def getNewestItem(self):
        return barcodeScanner.currentName

    def getNewestPrice(self):
        return barcodeScanner.currentPrice

    def getItems(self):

        return shoppingListItems

    def getPrices(self):

        return shoppingListPrices

    def getTotalPrice(self):

        return totalPrice

    def getScannedLength(self):

        return scannedLength
