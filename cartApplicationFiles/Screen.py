class Screen():

    import tkinter
    import Customer
    
    def __init__(self):

        shopper = Customer()
        scannedLength = shopper.getScannedLength()

        # Sets up the main widget
        root = Tk()
        root.geometry("480x320")
        root.title("Semi-Autonomous Shopping Cart")

        # Sets up section titles
        titlescanned = Label(root,text = "Scanned Products:")
        titleprice = Label(root,text = "Price")

        # Sets up scrollbar
        scrollbar = Scrollbar(root, orient=VERTICAL, command=yview)

        # Sets up grocery names and prices listboxes
        groceryListNames = Listbox(root, width = 50, height = 15,yscrollcommand=scrollbar.set)
        groceryListPrice = Listbox(root, width = 7, height = 15,yscrollcommand=scrollbar.set, justify=RIGHT)

        # Sets up remove button
        removeButton = Button(root, text= "Remove Selected", height=4, width = 25, command=callback)

        # Sets up the price output as an empty button
        groceryListTotal = Listbox(root, width = 7, height = 1, justify=RIGHT)

    # this has to be done to make the scrollbar work on both listboxes
    def yview(*args):
        groceryListNames.yview(*args)
        groceryListPrice.yview(*args)
            
    # callback is called when removeButton is clicked
    def callback():
        
        try:
            selected = groceryListNames.curselection()
            groceryListNames.delete(selected)
            groceryListPrice.delete(selected)
            groceryListTotal.delete(0,END)

            shopper.removeItem(int(selected))
            
            allPrices = shopper.getTotalPrice()

            groceryListTotal.delete(0,END)
            groceryListTotal.insert(END,"{0:.2f}".format(allPrices))
            
        except:
            pass

    def newValue(self):
        groceryListNames.insert(END, shopper.getNewestItem())
        groceryListPrice.insert(END, float(shopper.getNewestPrice))

    def draw(self):

        if(scannedLength == shopper.getScannedLength):
                    
            titlescanned.grid(row=0,column=0)
            titleprice.grid(row=0,column=1)
            scrollbar.grid(row=1,column=2,sticky = "ns")
            groceryListNames.grid(row=1,column=0)
            groceryListPrice.grid(row=1,column=1)
            groceryListTotal.grid(row=2,column=1,sticky = "n")
            removeButton.grid(row=2,column=0,rowspan=2)

            root.update()

        else:

            self.newValue()
        

    
        
