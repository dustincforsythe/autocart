import threading
from tkinter import *
import sys


class BarcodeScanner:

    def __init__(self):
        storeCatalog = {"640522710850": ["Raspberry Pi 3: Model B", "34.99"],
                "022000020734": ["Extra Chewy Mints: Polar Ice", "2.41"]
                }
        currentCode = ""
        currentPrice = 0
        currentName = ""

        shoppingListItems = []
        shoppingListPrices = []
        totalPrice = 0.0
        self.scannedLength = 4
        

        # Sets up the main widget
        root = Tk()
        root.geometry("480x320")
        root.title("Semi-Autonomous Shopping Cart")

        # Sets up section titles
        titlescanned = Label(root,text = "Scanned Products:")
        titleprice = Label(root,text = "Price")

        # Sets up scrollbar
        scrollbar = Scrollbar(root, orient=VERTICAL, command=self.yview)

        # Sets up grocery names and prices listboxes
        groceryListNames = Listbox(root, width = 50, height = 15,yscrollcommand=scrollbar.set)
        groceryListPrice = Listbox(root, width = 7, height = 15,yscrollcommand=scrollbar.set, justify=RIGHT)

        # Sets up remove button
        removeButton = Button(root, text= "Remove Selected", height=4, width = 25, command=self.callback)

        # Sets up the price output as an empty button
        groceryListTotal = Listbox(root, width = 7, height = 1, justify=RIGHT)

    def readBarcode(self):
        code = input("Scan a barcode: ")
        code = str(code)
        self.currentCode = code

    def lookupName(self):
        try:
            self.currentName = storeCatalog.get(currentCode)[0]
        except:
            print("Barcode not recognized")

    def lookupPrice(self):
        try:
            self.currentPrice = float(storeCatalog.get(currentCode)[1])
        except:
            print("Barcode not recognized")

    def getName(self):
        self.readBarcode()
        self.lookupName()
        return currentName

    def getPrice(self):
        self.readBarcode()
        lookupPrice()
        return currentPrice

    def addItem(self):
        shoppingListItems.append(getName())
        shoppingListPrices.append(getPrice())
        totalPrice = sum(shoppingListPrices)
        scannedLength += 1

    def removeItem(self,location):
        shoppingListItems.pop(location)
        shoppingListPrices.pop(location)
        totalPrice = sum(shoppingListPrices)
        scannedLength -= 1

    def getNewestItem(self):
        return currentName

    def getNewestPrice(self):
        return currentPrice

    def getItems(self):
        return shoppingListItems

    def getPrices(self):
        return shoppingListPrices

    def getTotalPrice(self):
        return totalPrice

    def getScannedLength(self):
        return self.scannedLength

    def newValue(self):
        groceryListNames.insert(END, getNewestItem())
        groceryListPrice.insert(END, float(getNewestPrice))

    # this has to be done to make the scrollbar work on both listboxes
    def yview(*args):
        groceryListNames.yview(*args)
        groceryListPrice.yview(*args)




    # callback is called when removeButton is clicked
    def callback():
        try:
            selected = groceryListNames.curselection()
            groceryListNames.delete(selected)
            groceryListPrice.delete(selected)
            groceryListTotal.delete(0,END)

            removeItem(int(selected))
            
            allPrices = getTotalPrice()

            groceryListTotal.delete(0,END)
            groceryListTotal.insert(END,"{0:.2f}".format(allPrices))
            
        except:
            pass



    def draw(self):

        if(self.getScannedLength() == self.groceryListNames.size()):
                    
            titlescanned.grid(row=0,column=0)
            titleprice.grid(row=0,column=1)
            scrollbar.grid(row=1,column=2,sticky = "ns")
            groceryListNames.grid(row=1,column=0)
            groceryListPrice.grid(row=1,column=1)
            groceryListTotal.grid(row=2,column=1,sticky = "n")
            removeButton.grid(row=2,column=0,rowspan=2)

            root.update()

        else:

            self.newValue()
