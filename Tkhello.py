from tkinter import *

# callback is called when removeButton is clicked
def callback():
    try:
        selected = groceryListNames.curselection()
        groceryListNames.delete(selected)
        groceryListPrice.delete(selected)
    except:
        pass
# this has to be done to make the scrollbar work on both listboxes
def yview(*args):
    groceryListNames.yview(*args)
    groceryListPrice.yview(*args)

# Sets up the main widget
root = Tk()
root.geometry("480x320")
root.title("Semi-Autonomous Shopping Cart")

# Sets up section titles
titlescanned = Label(root,text = "Scanned Products:")
titleprice = Label(root,text = "Price")

# Sets up scrollbar
scrollbar = Scrollbar(root, orient=VERTICAL, command=yview)

# Sets up grocery names and prices listboxes
groceryListNames = Listbox(root, width = 50, height = 15,yscrollcommand=scrollbar.set)
groceryListPrice = Listbox(root, width = 7, height = 15,yscrollcommand=scrollbar.set)

# Sets up remove button
removeButton = Button(root, text= "Remove Selected", height=4, width = 25, command=callback)

# Sets up the price output as an empty button
groceryListTotal = Listbox(root, width = 7, height = 1)

# Insert temporary values
groceryListNames.insert(END,"Raspberry Pi 3: Model B")
groceryListPrice.insert(END,"34.99")



titlescanned.grid(row=0,column=0)
titleprice.grid(row=0,column=1)
scrollbar.grid(row=1,column=2,sticky = "ns")
groceryListNames.grid(row=1,column=0)
groceryListPrice.grid(row=1,column=1)
groceryListTotal.grid(row=2,column=1,sticky = "n")
removeButton.grid(row=2,column=0,rowspan=2)


root.mainloop()
