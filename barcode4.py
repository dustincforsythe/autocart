﻿
#!/usr/bin/python
import sys
#import requests
#import json

shoppingListItems = []
shoppingListPrices = []
totalPrice = 0.0

storeCatalog = {"640522710850": ["Raspberry Pi 3: Model B", "34.99"],
                "022000020734": ["Extra Chewy Mints: Polar Ice", "2.41"]
                }

def barcode_reader():
    code = input("Scan a barcode: ")
    code = str(code)
    return code

def UPC_lookup(upc):
    
    if upc in storeCatalog:
        return storeCatalog.get(upc)
    

if __name__ == '__main__':
    try:
        while True:

            currentScan = UPC_lookup(barcode_reader())

            shoppingListItems.append(currentScan[0])
            shoppingListPrices.append(float(currentScan[1]))
            totalPrice = sum(shoppingListPrices)

            print(shoppingListItems)
            print(totalPrice)
            
    except KeyboardInterrupt:
        print("ERROR")
        pass
