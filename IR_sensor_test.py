import serial

ser = serial.Serial('/dev/ttyACM0',9600,timeout=1)

while 1:
    val = ser.readline().decode('utf-8')
    parsed = val.split(',')
    parsed = [x.rstrip() for x in parsed]

    if(len(parsed)==2):
        
        if(parsed[0] == "lefton"):
            leftstatus = True
        else:
            leftstatus = False

        if(parsed[1] == "righton"):
            rightstatus = True
        else:
            rightstatus = False

        if(leftstatus and rightstatus):
            print("drive forward")
        elif(leftstatus and not rightstatus):
            print("turn left")
        elif(not leftstatus and rightstatus):
            print("turn right")
        elif(not leftstatus and not rightstatus):
            print("stop")


        
