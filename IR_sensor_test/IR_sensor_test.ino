int leftSensor = 15;
int rightSensor = 14;

void setup(){
  Serial.begin(9600);
  pinMode(leftSensor, INPUT);
  pinMode(rightSensor, INPUT);
  
}

void loop(){
  
  String data = "";
  
  if (digitalRead(leftSensor)==1){
    data = data + "leftoff";
  }
  else{
    data = data + "lefton";
  }
  
  if (digitalRead(rightSensor)==1){
    data = data + ",rightoff";
  }
  else{
    data = data + ",righton";
  }
  
  Serial.println(data);
  
}
