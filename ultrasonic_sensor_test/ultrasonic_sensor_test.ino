int nTrig = 7;
int nEcho = 6;
int nDuration = 0;
int nDistance = 0;

int neTrig = 5;
int neEcho = 4;
int neDuration = 0;
int neDistance = 0;

int nwTrig = 8;
int nwEcho = 9;
int nwDuration = 0;
int nwDistance = 0;

int seTrig = 19;
int seEcho = 18;
int seDuration = 0;
int seDistance = 0;

int swTrig = 2;
int swEcho = 3;
int swDuration = 0;
int swDistance = 0;

void setup(){
  Serial.begin(9600);
  
  pinMode(nTrig, OUTPUT);
  pinMode(neTrig, OUTPUT);
  pinMode(nwTrig, OUTPUT);
  pinMode(seTrig, OUTPUT);
  pinMode(swTrig, OUTPUT);
  
  pinMode(nEcho, INPUT);
  pinMode(neEcho, INPUT);
  pinMode(nwEcho, INPUT);
  pinMode(seEcho, INPUT);
  pinMode(swEcho, INPUT);
  
  digitalWrite(nTrig, LOW);
  digitalWrite(neTrig, LOW);
  digitalWrite(nwTrig, LOW);
  digitalWrite(seTrig, LOW);
  digitalWrite(swTrig, LOW);
  
}

void loop(){
  
  digitalWrite(nTrig, HIGH);
  delayMicroseconds(10);
  digitalWrite(nTrig, LOW);
  nDuration = pulseIn(nEcho,HIGH);
  nDistance = nDuration/58.2;
  
  digitalWrite(neTrig, HIGH);
  delayMicroseconds(10);
  digitalWrite(neTrig, LOW);
  neDuration = pulseIn(neEcho,HIGH);
  neDistance = neDuration/58.2;
  
  digitalWrite(seTrig, HIGH);
  delayMicroseconds(10);
  digitalWrite(seTrig, LOW);
  seDuration = pulseIn(seEcho,HIGH);
  seDistance = seDuration/58.2;
  
  digitalWrite(swTrig, HIGH);
  delayMicroseconds(10);
  digitalWrite(swTrig, LOW);
  swDuration = pulseIn(swEcho,HIGH);
  swDistance = swDuration/58.2;
  
  digitalWrite(nwTrig, HIGH);
  delayMicroseconds(10);
  digitalWrite(nwTrig, LOW);
  nwDuration = pulseIn(nwEcho,HIGH);
  nwDistance = nwDuration/58.2;
  
  Serial.print(nDistance); Serial.print(",");
  Serial.print(neDistance); Serial.print(",");
  Serial.print(seDistance); Serial.print(",");
  Serial.print(swDistance); Serial.print(","); 
  Serial.println(nwDistance);
  
  delay(500);
  
}
