from tkinter import *
import tkinter.font
import sys

storeCatalog = {"640522710850": ["Raspberry Pi 3: Model B", "34.99"],
                "022000020734": ["Extra Chewy Mints: Polar Ice", "2.41"]
                }

# callback is called when removeButton is clicked
def callback():
    try:
        selected = groceryListNames.curselection()
        groceryListNames.delete(selected)
        groceryListPrice.delete(selected)
        groceryListTotal.delete(0,END)
        allPrices = sum(map(float,list(groceryListPrice.get(0,END))))

        groceryListTotal.delete(0,END)
        groceryListTotal.insert(END,"{0:.2f}".format(allPrices))
        
    except:
        pass
    
def clearBarcodeEntry():
    try:
        textEntry.delete(0,END)
        
    except:
        pass
def submitCode():
    try:
        enteredcode = textEntry.get()
        print(enteredcode)
        if(enteredcode in storeCatalog.keys()):
            groceryListNames.insert(END, storeCatalog.get(enteredcode)[0])
            groceryListPrice.insert(END, storeCatalog.get(enteredcode)[1])
           
            allPrices = sum(map(float,list(groceryListPrice.get(0,END))))

            groceryListTotal.delete(0,END)
            groceryListTotal.insert(END,"{0:.2f}".format(allPrices))
            
        else:
            textEntry.delete(0,END)
            textEntry.insert(0, "Item not found.")
        
    except:
        pass
def exitfunction():
    root.destroy()

# Sets up the main widget
root = Tk()
root.attributes("-fullscreen",True)
#root.geometry("480x320")
#root.title("Semi-Autonomous Shopping Cart")

# Sets up section titles
titlescanned = Label(root,text = "Scanned Products:")
titleprice = Label(root,text = "Price")

# Sets up grocery names and prices listboxes
groceryListNames = Listbox(root, width = 50, height = 15)
groceryListPrice = Listbox(root, width = 9, height = 15)

# Sets up text entry
textEntry = Entry(root,width=26)
textEntry.delete(0,END)
textEntry.insert(0, "Enter a barcode Here")

# Sets up remove button
removeButton = Button(root, text= "Remove Selected", height=4, width = 20, command=callback)

# Sets up clear button
clearButton = Button(root, text= "Clear Text", height=2, width = 10, command=clearBarcodeEntry)

# Sets up submit button
submitButton = Button(root, text= "Add Item", height=2, width = 10, command=submitCode)

# Sets up exit button
exitButton = Button(root, text= "Exit", height=2, width = 3, command=exitfunction)

# Sets up the price output as an empty button
groceryListTotal = Listbox(root, width = 9, height = 1, justify=RIGHT)



allPrices = sum(map(float,list(groceryListPrice.get(0,END))))

groceryListTotal.delete(0,END)
groceryListTotal.insert(END,"{0:.2f}".format(allPrices))




titlescanned.grid(row=0,column = 0,columnspan=3)
titleprice.grid(row=0,column=3)
groceryListNames.grid(row=1,column=0,columnspan=3)
groceryListPrice.grid(row=1,column=3)
removeButton.grid(row=2,column=0,rowspan=2)
textEntry.grid(row=2,column=1,columnspan=2)
groceryListTotal.grid(row=2,column=3)
clearButton.grid(row=3,column=1)
submitButton.grid(row=3,column=2)
exitButton.grid(row=3,column=3)

if __name__ == '__main__':
    try:
        while True:
            """
            currentScan = UPC_lookup(barcode_reader())

            shoppingListItems.append(currentScan[0])
            shoppingListPrices.append(float(currentScan[1]))
            totalPrice = sum(shoppingListPrices)
            """
   
            root.update()
            
            #print(shoppingListItems)
            #print(totalPrice)
            
    except KeyboardInterrupt:
        print("ERROR")
        pass
