import serial

ser = serial.Serial('/dev/ttyACM0',9600,timeout=1)

while 1:
    val = ser.readline().decode('utf-8')
    parsed = val.split(',')
    parsed = [x.rstrip() for x in parsed]
    if(len(parsed)>4):
        nDist = abs(int(int(parsed[0] + '0')/10))
        neDist = abs(int(int(parsed[1] + '0')/10))
        seDist = abs(int(int(parsed[2] + '0')/10))
        swDist = abs(int(int(parsed[3] + '0')/10))
        nwDist = abs(int(int(parsed[4] + '0')/10))
        print(" ")
        print("North      Distance to Object (cm): " + str(nDist))
        print("North East Distance to Object (cm): " + str(neDist))
        print("South East Distance to Object (cm): " + str(seDist))
        print("South West Distance to Object (cm): " + str(swDist))
        print("North West Distance to Object (cm): " + str(nwDist))
        print(" ")

        
